# -*- text -*-
######################################################################
#
#	This is a virtual server that handles *only* inner tunnel
#	requests for EAP-TTLS and PEAP types.
#
#	$Id: 2c6f9611bfc7b4b782aeb9764e47e832690739c4 $
#
######################################################################

server eduroam-inner-tunnel {

#
#  This next section is here to allow testing of the "inner-tunnel"
#  authentication methods.
#
listen {
       ipaddr = 127.0.0.1
       port = 18120
       type = auth
}

#
#  Authorization.
#
authorize {

	#  eduroam log.
	auth_log

	#
	#  This module takes care of EAP-MSCHAPv2 authentication.
	eap-eduroam {
		ok = return
	}

	#
	#  The "suffix" module takes care of stripping the domain
	#  (e.g. "@example.com") from the User-Name attribute.
	suffix

	#
	# Strip User-Name within realm.
	if ("%{request:User-Name}" =~ /^(.*)@(.*)/) {
		update request {
			Stripped-User-Name := "%{1}"
			Realm := NULL
		}
		update control {
			Proxy-To-Realm := LOCAL
		}
	}

	#
	# Group of authorize modules.
	group {
		#
		#  Read the 'users-eduroam' file.
		files-eduroam {
			# return if match
			ok = return
			updated = return
		}

		#
		#  for LDAP module.
		ldap-eduroam {
		#	# return if match
			ok = return
			updated = return
		}

		#
		#  for Active Directory or mschap module.
		#mschap-eduroam {
		#	# return if match
		#	ok = return
		#	updated = return
		#}

		#
		#  for MySQL or sql module.
		sql-eduroam {
			# return if match
			ok = return
			updated = return
		}

		#
		# return reject if not match any modules.
		notfound = reject
	}

	#
	#  If no other module has claimed responsibility for
	#  authentication, then try to use PAP.
	pap

}

#
#  Authentication.
#
authenticate {
	#
	#  PAP authentication.
	Auth-Type PAP {
		pap
	}

	#
	#  CHAP authentication.
	Auth-Type CHAP {
		chap
	}

	#
	#  MSCHAP authentication.
	#  for file-eduroam and/or LDAP and/or MySQL
	Auth-Type MS-CHAP {
		mschap
	}

	#
	#  MSCHAP authentication.
	#  for Active Directory
	#Auth-Type MS-CHAP {
	#	mschap-eduroam
	#}

	#
	#  Allow EAP authentication.
	eap-eduroam
}

######################################################################
#
#	There are no accounting requests inside of EAP-TTLS or PEAP
#	tunnels.
#
######################################################################

#
#  Session.
#
session {

}

#
#  Post-Authentication.
#
post-auth {

	update reply {
		User-Name := "%{request:User-Name}"
	}

	#
	#  Log of authentication replies.
	reply_log

	#
	#  Access-Reject packets are sent through the REJECT sub-section of the
	#  post-auth section.
	Post-Auth-Type REJECT {
		attr_filter.access_reject

		update outer.session-state {
			&Module-Failure-Message := &request:Module-Failure-Message
		}
	}

	update outer.reply {
		User-Name = "%{request:User-Name}"
	}

}

#
#  Pre-proxy.
#
pre-proxy {

}

#
#  Post-proxy.
#
post-proxy {

}

} # server eduroam-inner-tunnel {}
